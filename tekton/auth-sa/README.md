### ServiceAccount for Tekton (tekton-admin)
- needs proper permissions to ...
  - get or create pod, rs, deploy etc
  - get or create tekton.dev, triggers.tekton (custom resources)
- needs credentials of ...
  - code repository (gitlab)
  - contaienr image registry (gitlab)
  - webhook token (gitlab)
- ... in 'test' namespace.

Since this manifests repository will be published in public, some critical credentials are managed by "ExternalSecret", not by vanilla "Secret".
https://github.com/external-secrets/kubernetes-external-secrets