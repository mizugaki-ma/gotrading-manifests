package main

name = input.metadata.name

required_deployment_selectors {
  input.spec.selector.matchLabels.app
  input.spec.selector.matchLabels.version
}

required_deployment_labels {
    input.metadata.labels["app.kubernetes.io/name"]
    input.metadata.labels["app.kubernetes.io/instance"]
    input.metadata.labels["app.kubernetes.io/version"]
    input.metadata.labels["app.kubernetes.io/component"]
    input.metadata.labels["app.kubernetes.io/part-of"]
    input.metadata.labels["app.kubernetes.io/managed-by"]
}

warn[msg] {
  input.kind = "Deployment"
  not required_deployment_selectors
  msg = sprintf("[Conftest-warn]You should set proper selectors to Deploy. %s", [name])
}


deny[msg] {
  input.kind = "Deployment"
  not required_deployment_labels
  msg = sprintf("[Conftest-deny]You must set proper labels to Deploy. %s", [name])
}

deny[msg] {
  input.kind = "Deployment"
  not input.spec.template.spec.securityContext.runAsNonRoot
  msg = sprintf("[Conftest-deny]You must run pod as non-root user. %s", [name])
}